(() => {
    const endTower = document.querySelector('#end');
    const modal = document.querySelector('.modal');
    const buttonReset = document.querySelector('#buttonReset');
    const timer = document.querySelector('#tempo');
    let time = [0, 0, 0];
    let intervalID = 0;
    let contadorDeMovimentos = 0;
    let torreDeOrigem = '';

    const incrementarContador = () => {
        document.querySelector('#movimentos').innerText = ++contadorDeMovimentos;
    };

    const zeroLeft = (number) => {
        if (number <= 9) {
            return '0' + number;
        }

        return number.toString();
    };

    const showTimer = () => {
        timer.innerText = `${zeroLeft(time[0])}:${zeroLeft(time[1])}:${zeroLeft(time[2])}`

        return timer.innerText;
    }

    const incrementarTimer = () => {
        time[2]++;

        if (time[2] === 60) {
            time[2] = 0;
            time[1]++;
        }

        if (time[1] === 60) {
            time[1] = 0;
            time[0]++;
        }

        showTimer();
    };

    const inicializaTimer = () => {
        intervalID = setInterval(incrementarTimer, 1000);
        console.log(intervalID);
    };

    const finalizaTimer = () => {
        clearInterval(intervalID);
        intervalID = 0;
    };

    const exibirResultado = () => {
        modal.classList.remove('hidden');

        document.querySelector('#totalMovimentos').innerText = contadorDeMovimentos;

        document.querySelector('#totalTempo').innerText = showTimer();
    };

    const removerResultado = () => {
        reiniciar();

        modal.classList.add('hidden');
    };

    const movimentar = (event) => {
        const torreAtual = event.currentTarget;

        if (intervalID === 0) {
            inicializaTimer();
        }

        if (buttonReset.classList.contains('disabled')) {
            buttonReset.classList.remove('disabled');
        }

        if (torreDeOrigem === '' && torreAtual.childElementCount === 0) {
            new Audio('./audios/error.flac').play();
        } else if (torreDeOrigem === '' && torreAtual.childElementCount !== 0) {
            new Audio('./audios/up.wav').play();

            torreAtual.classList.add('selected');

            torreDeOrigem = torreAtual;
        } else if (torreAtual.lastElementChild === null ||
            torreDeOrigem.lastElementChild.clientWidth <= torreAtual.lastElementChild.clientWidth) {
            new Audio('./audios/down.wav').play();

            torreAtual.appendChild(torreDeOrigem.lastElementChild);

            if (torreDeOrigem !== torreAtual) {
                incrementarContador();
            }

            if (endTower.childElementCount === 4) {
                finalizaTimer();
                exibirResultado();
            }

            torreDeOrigem.classList.remove('selected');
            torreDeOrigem = '';
        } else {
            new Audio('./audios/error.flac').play();
        }
    };

    const reiniciar = () => {
        const disco4 = document.querySelector('#disco4');
        const disco3 = document.querySelector('#disco3');
        const disco2 = document.querySelector('#disco2');
        const disco1 = document.querySelector('#disco1');

        const startTower = document.querySelector('#start');
        startTower.innerHTML = '';
        startTower.appendChild(disco4);
        startTower.appendChild(disco3);
        startTower.appendChild(disco2);
        startTower.appendChild(disco1);

        document.querySelectorAll('.tower').forEach(tower => {
            tower.classList.remove('selected');
        });

        if (intervalID !== 0) {
            finalizaTimer();
            intervalID = 0;
        }

        time = [0, 0, 0];
        showTimer();
        contadorDeMovimentos = -1;
        incrementarContador();
        torreDeOrigem = '';
        buttonReset.classList.add('disabled');
    };

    document.querySelectorAll('.tower').forEach(tower => {
        tower.addEventListener('click', movimentar);
    });

    buttonReset.addEventListener('click', reiniciar);

    document.querySelector('#buttonFechar').addEventListener('click', removerResultado);

    window.addEventListener('click', (event) => {
        if (event.target === modal) {
            removerResultado();
        }
    });

})();